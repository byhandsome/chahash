/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : md5

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2021-01-04 19:11:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `md5`
-- ----------------------------
DROP TABLE IF EXISTS `md5`;
CREATE TABLE `md5` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `md5_32` varchar(32) DEFAULT NULL COMMENT '32位密文',
  `md5_16` varchar(16) DEFAULT NULL COMMENT '16位密文',
  `result` varchar(255) NOT NULL COMMENT '解密结果',
  `type` varchar(255) DEFAULT NULL COMMENT '加密类型',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `md5_32` (`md5_32`) USING BTREE,
  KEY `md5_16` (`md5_16`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for `md5_fail`
-- ----------------------------
DROP TABLE IF EXISTS `md5_fail`;
CREATE TABLE `md5_fail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `md5` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `ua` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
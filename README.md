# chahash

#### 介绍
MD5解密网站源码

#### 软件架构
*  前端仿至SOMD5
*  内核开发者: ByHandsome QQ群 908322022
*  系统由PHP + Mysql 组成
*  开发时间: 2020年4月21日 
*  当前版本: V6.0版
*  运行速度: 2千6百万条数据 0.05秒读写完成。

#### 功能修复
1. 修复明文提交重复过滤
2. 新增MD5表项索引，大大提升读写速度。

#### 安装教程

1.  导入 dbsql\chahash.sql 数据库结构
2.  修改 include\config.php 数据库配置信息
3.  xxxx

#### API使用说明

1.  api.php?type=decrypt&hash= MD5密文
2.  api.php?type=submit&hash= 提交的明文
3.  api.php?type=totaldata  打印当前收录条数

#### 后台使用说明

将在新版本中展现，新版本采用TP6框架

#### 官网演示

[演示地址](https://www.chahash.com/)

#### 鸣谢开源项目

[验证码引用](https://gitee.com/hooray/clicaptcha?_from=gitee_search)


<?php
/*
*  MD5 Decrypt API模块。
*  ByHandsome 编写
*  本模块支持Mysql大数据分布式储存调用。
*  2020年4月26日 V6.0版
*/
error_reporting(0);//屏蔽错误,防止暴路径
header('Content-type:text/json');//输出内格式为JSON
require_once("./include/global.php");//引入扩展文件
$type = addslashes($_GET['type']);//此处type用于判断类型
if($type==''){
	$type = addslashes($_POST['type']);//此处提取GET提交的参数为hash。
}

$hash = addslashes($_GET['hash']);//此处提取GET提交的参数为hash。
if($hash==''){
	$hash = addslashes($_POST['hash']);//此处提取GET提交的参数为hash。
}

if(empty($type)){
	$json = array('code' => '404','msg' => '大兄弟,参数被你吃了么？','data' => 'error');
	echo json_encode($json,JSON_UNESCAPED_UNICODE);
}
/** 判断类型  **/
if($type=='decrypt'){
	/* 解密开始 */
	if(strlen($hash) == 16 ){
			//16位MD5解密
	        $sql = "select * from md5 where md5_16='".$hash."'";//查询SQL
	        $results=$db->query($sql);
	        $row=$db->fetch_array($results);
	        $result = $row['result'];
			if(empty($result)){
				//将失败的MD5记录下来。
				$sql = "INSERT INTO  `md5_fail` (`id` ,`md5`,`time`,`ip` ,`ua`,`os`,`type`)VALUES (NULL ,'".$hash."','".date("Y-m-d H:i:s")."','".get_ip()."','".get_browsers()."','".get_os()."','api');";//查询SQL
				$results=$db->query($sql);
				$row=$db->fetch_array($results);
				$json = array('code' => '200','msg' => 'MD5解密失败','data' => 'fail');
			}else{
				$json = array('code' => '200','msg' => 'Success','data' => $result);
			}
			
	}elseif(strlen($hash) == 32 ){
		//32位MD5解密
		$sql = "select * from md5 where md5_32='".$hash."'";//查询SQL
		$results=$db->query($sql);
		$row=$db->fetch_array($results);
		$result = $row['result'];
	    if(empty($result)){
			//将失败的MD5记录下来。
			$sql = "INSERT INTO  `md5_fail` (`id` ,`md5`,`time`,`ip` ,`ua`,`os`,`type`)VALUES (NULL ,'".$hash."','".date("Y-m-d H:i:s")."','".get_ip()."','".get_browsers()."','".get_os()."','api');";//查询SQL
			$results=$db->query($sql);
			$row=$db->fetch_array($results);
			$json = array('code' => '200','msg' => 'MD5解密失败','data' => 'fail');
		}else{
			$json = array('code' => '200','msg' => 'Success','data' => $result);
	    }
		
    }else{
		$json = array('code' => '404','msg' => 'MD5格式错误','data' => 'error');
    }
	echo json_encode($json,JSON_UNESCAPED_UNICODE);
	/* 解密结束 */
}
//输出总数据条数
elseif($type=='totaldata'){
	$sql = 'select count(*) from md5';//执行SQL语句
	$results=$db->query($sql);
	$row=$db->fetch_array($results);
	$data  = $row['count(*)']; //条数
	echo json_encode(array('code' => '200','msg' => '已有'.$data.'条'),JSON_UNESCAPED_UNICODE);
}
//明文提交
elseif($type=='submit'){
	if($hash != null){
		//判断是否重复
		$sql = "select * from md5 where result='".$hash."'";//查询SQL
		$results=$db->query($sql);
		$row=$db->fetch_array($results);
		$result = $row['result'];
		if($result != null){
			$json = array('code' => '404','msg' => 'error','data' => '提交失败,因为已收录。');
		}else{
			//写入数据库
			$sql = "INSERT INTO `md5`(`id`, `md5_32`, `md5_16`, `result`) VALUES (NULL ,'".md5($hash)."','".substr(md5($hash),8,16)."','".$hash."')";
			$results=$db->query($sql);
			$row=$db->fetch_array($results);
			$json = array('code' => '200','msg' => 'Success','data' => '感谢提交');
		}
	}
	echo json_encode($json,JSON_UNESCAPED_UNICODE);
}
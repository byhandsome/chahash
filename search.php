<?php
/*
*  MD5解密模块。
*  ByHandsome 编写
*  本模块自动识别PHP版本并执行相关语句。
*  2020年4月26日  V6.0版
*/
error_reporting(0);//屏蔽错误,防止暴路径
header('Content-Type:application/json; charset=utf-8');
require_once("./include/global.php");//引入扩展文件
$hash = addslashes($_POST['hash']);//提取POST提交的Hash。
$captcha =$_POST['captcha'];//验证码特征。
//防止报错伪验证码造特征
if($captcha == '0'){
	$captcha ='74,31;350;200';
	exit('{"err":5}');
}
//验证码初始化
$clicaptcha = new clicaptcha();
$typ=$clicaptcha->check($_POST['captcha']) ? '1' : '0';
if($hash!=''){
	if($typ=='1'){
	if(strlen($hash) == 16 ){
			//16位MD5解密
			$t1 = microtime(true);
	        $sql = "select * from md5 where md5_16='".$hash."'";//查询SQL
			$result=$db->query($sql);
			$row=$db->fetch_array($result);
	        $express = $row['result'];

	if($express == null){
			//将失败的MD5记录下来。
	        $sql = "INSERT INTO  `md5_fail` (`id` ,`md5`,`time`,`ip` ,`ua`,`os`,`type`)VALUES (NULL ,'".$hash."','".date("Y-m-d H:i:s")."','".get_ip()."','".get_browsers()."','".get_os()."','md5');";//查询SQL
			$result=$db->query($sql);
			$row=$db->fetch_array($result);
			echo json_encode(array('err' => 3 ));

		}else{
			echo json_encode(array(
			'data' => $express,
			'err'  => 0,
			'time'  => microtime(true) - $t1,
			'type' => 'md5'
			));
			
		}
		
		}elseif(strlen($hash) == 32 ){
			//32位MD5解密
			$t1 = microtime(true);
		    $sql = "select * from md5 where md5_32='".$hash."'";//查询SQL
			$result=$db->query($sql);
			$row=$db->fetch_array($result);
		    $express = $row['result'];
		
	if($express == null){
			//将失败的MD5记录下来。
	        $sql = "INSERT INTO  `md5_fail` (`id` ,`md5`,`time`,`ip` ,`ua`,`os`,`type`)VALUES (NULL ,'".$hash."','".date("Y-m-d H:i:s")."','".get_ip()."','".get_browsers()."','".get_os()."','md5');";//查询SQL
	        $result=$db->query($sql);
	        $row=$db->fetch_array($result);
	        echo json_encode(array('err' => 3 ));

	}else{
		echo json_encode(array('data' => $express,'err'  => 0,'time'  => microtime(true) - $t1,'type' => 'md5'));
		
		}
	}else{
		if($hash<>null){
			echo json_encode(array('err' => 2 ));	
		}
	}
	
 }elseif($typ=='0'){
	echo json_encode(array('err' => 5 ));
  }
}else{
	echo json_encode(array('err' => 5 ));
}